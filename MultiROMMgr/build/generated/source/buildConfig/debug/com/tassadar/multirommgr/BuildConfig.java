/**
 * Automatically generated file. DO NOT MODIFY
 */
package com.tassadar.multirommgr;

public final class BuildConfig {
  public static final boolean DEBUG = Boolean.parseBoolean("true");
  public static final String APPLICATION_ID = "com.tassadar.multirommgr.debug";
  public static final String BUILD_TYPE = "debug";
  public static final String FLAVOR = "";
  public static final int VERSION_CODE = 188;
  public static final String VERSION_NAME = "1.188-debug";
}
